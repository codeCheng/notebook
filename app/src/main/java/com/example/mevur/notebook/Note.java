package com.example.mevur.notebook;

import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;

public class Note extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
    }

    public void savefile(View view) {
        EditText note = (EditText) findViewById(R.id.edt_note);
        String noteTxt = note.getText().toString();
        try {

            File file = new File(getFilesDir(), "note.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            Log.i("file", file.getPath());
            FileWriter writer = new FileWriter(file);
            writer.append(noteTxt);
            writer.flush();
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void open(View view) {
        File file = new File(getFilesDir(),"note.txt");
        try {
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = "";
                StringBuffer sb = new StringBuffer();
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                EditText note = (EditText) findViewById(R.id.edt_note);
                note.setText(sb.toString());

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
